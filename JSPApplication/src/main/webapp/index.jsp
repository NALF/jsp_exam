<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
        String contextPath = request.getContextPath();
        String message = "Hello World of JSPs";
        String title = "JSPs Application";
    %>
    
    <%!
    public String getDate(){
        return java.util.Calendar.getInstance().getTime().toString();
    }
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><%= title %></title>
</head>
<body>
    <%= message %>
    <br>
    <h1>Examples: </h1>
    <br>
    <ul>
        <li>---------------------</li>
        <li><a href="<%= contextPath %>/views/welcomeapp.jsp">Welcome Page</a></li>
        <li><a href="<%= contextPath %>/views/getForm.jsp">Get Form Page</a></li>
        <li><a href="<%= contextPath %>/configObjectExample">Config Object Example</a></li>
        <li><a href="<%= contextPath %>/views/SessionObjectExample.jsp">Session Object Example</a></li>
        <li><a href="<%= contextPath %>/views/IncludeDirective.jsp">Include Directive Example</a></li>
        <li><a href="<%= contextPath %>/views/PageDirectiveExample.jsp">Page Directive Example</a></li>
        <li><a href="<%= contextPath %>/views/ExceptionHandlingExample.jsp">Exception Handling Example</a></li>
        <li><a href="<%= contextPath %>/views/LoginForm.jsp">Using Controllers Example</a></li>
        <li><a href="<%= contextPath %>/views/NewActivity.jsp">Go to my To Do List</a></li>
        
       
                
                
        
        
        
    </ul>
    <br>
    <%= getDate() %>
</body>
</html>