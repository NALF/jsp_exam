<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
        String contextPath = request.getContextPath();
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new Activity</title>
</head>
<body>
<form action="../ActivityServlet" method="POST">
<h2>ADD NEW ACTIVITY</h2>
<textarea rows = "5" cols = "40" maxlength = "50" name = "activity">
         </textarea>
<button type="submit">Add</button>
</form>
</body>
<footer><a href="<%= contextPath %>/views/ShowActivities.jsp">Pending Activities</a> &nbsp;&nbsp;&nbsp;<a href="<%= contextPath %>/views/DoneActivities.jsp">Done Activities</a>
</footer>
</html>