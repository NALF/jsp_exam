package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.controller.LoginBean;
import com.softtek.academy.javaweb.service.MyConnection;

public class LoginDao {

	
	
	public static LoginBean getUser(String name) {
		LoginBean user = null;
		try {
			Connection conn =MyConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM USER WHERE NAME=?");
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				user = new LoginBean();
				user.setName(rs.getString("NAME"));
				user.setPasswd(rs.getString("PASS"));
			}
		}catch(Exception e) {
			System.out.println("---------------------------------------");
			System.out.println(e);
			return user;
		}
		return user;
	}
	public static List<String> getColors(){
		List<String> lista = new ArrayList<String>();
		try {
			Connection conn = MyConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM COLOR");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				lista.add(rs.getString("NAME"));
				System.out.println(rs.getString("NAME"));
			}
		}catch(Exception e) {
			System.out.println(e+"ERROR");
			return lista;
		}
		
		return lista;
	}
}
