package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.controller.TDListBean;
import com.softtek.academy.javaweb.service.ActivitiesService;
import com.softtek.academy.javaweb.service.MyConnection;

public class TDListDAO {
	
	public static List<TDListBean> getActivities(){
		List<TDListBean> lista = ActivitiesService.getActivities();
		return lista;
	}
	public static void addActivity(String act) {
		ActivitiesService.addActivity(act);
		
	}
	public static void DonedActivity(int id) {
		ActivitiesService.DonedActivity(id);
	}
	public static List<TDListBean> getDonedActivities(){
		List<TDListBean> lista = ActivitiesService.getDonedActivities();
		return lista;
	}
	
	
	
}
