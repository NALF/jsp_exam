package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.dao.TDListDAO;

/**
 * Servlet implementation class ActivityServlet
 */
public class ActivityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActivityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.sendRedirect("/JSPApplication/views/ShowActivities.jsp");
		System.out.println(request.getParameter("done"));
		int id= Integer.parseInt(request.getParameter("done"));
		TDListDAO.DonedActivity(id);
		response.sendRedirect("/JSPApplication/views/DoneActivities.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String act = request.getParameter("activity");
		TDListDAO.addActivity(act);
		response.sendRedirect("/JSPApplication/views/NewActivity.jsp");

	}

}
