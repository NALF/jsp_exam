package com.softtek.academy.javaweb.controller;

public class TDListBean {
	String act;
	int id;
	int status;
	
	
	public TDListBean(String act, int id, int status) {
		this.act = act;
		this.id=id;
		this.status = status;
	}
	public String getAct() {
		return act;
	}
	public void setAct(String act) {
		this.act = act;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
