package com.softtek.academy.javaweb.controller;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.dao.LoginDao;

/**
 * Servlet implementation class ControllerServlet
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String psswd;
	String username;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		psswd = request.getParameter("password");
		username =request.getParameter("username");
		
		/*LoginBean bean = new LoginBean(username,psswd);
		request.setAttribute("bien", bean.validate());*/
		
		LoginBean bean = new LoginBean();
		bean.setName(username);
		bean.setPasswd(psswd);
		request.setAttribute("bean", bean);
		
		boolean status = bean.validate(bean);
		request.setAttribute("bien", status);
		
		if(status) {
			response.sendRedirect("/JSPApplication/views/LoginGood.jsp");
		}
		else {
			response.sendRedirect("/JSPApplication/views/LoginBAD.jsp");
		}
		List<String> colors =LoginDao.getColors();
		request.setAttribute("colors", colors);
		
		 /*if(status){
		 	RequestDispatcher rd = request.getRequestDispatcher("/views/LoginGood.jsp");
		 	rd.forward(request,response);
		 }else{
		 	RequestDispatcher rd  = request.getRequestDispatcher("/views/LoginBAD.jsp");
		 	rd.forward(request,response);
		 	}*/
		 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
