package com.softtek.academy.javaweb.dao;


public class ColorDao {
	 long id;
     String name;
     String hexValue;
     
     public ColorDao(long id, String name, String hexValue) {
 		this.id = id;
 		this.name = name;
 		this.hexValue = hexValue;
 	}
     
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHexValue() {
		return hexValue;
	}
	public void setHexValue(String hexValue) {
		this.hexValue = hexValue;
	}

	@Override
	public String toString() {
		return "Color [id=" + id + ", name=" + name + ", hexValue=" + hexValue + "]";
	}
     
     
}