package com.softtek.academy.javaweb.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.controller.TDListBean;

public class ActivitiesService {
	public static  List<TDListBean> getActivities(){
		List<TDListBean> lista = new ArrayList<TDListBean>();
		try {
			Connection conn = MyConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM to_do_list where is_done=0");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				lista.add(new TDListBean(rs.getString("list"),rs.getInt("id"),rs.getInt("is_done")));
			}
		}catch(Exception e) {
			System.out.println(e+"ERROR");
			return lista;
		}
		
		return lista;
	}
	public static boolean addActivity(String act) {
		try {
			Connection conn = MyConnection.getConnection();
			/*Statement st =conn.createStatement();
			st.executeUpdate("INSERT INTO to_do_list (list,is_done) values ("+act+","+0+")");*/
			PreparedStatement ps = conn.prepareStatement("INSERT INTO to_do_list (list) values (?)");
			ps.setString(1, act);
            ps.execute();
            return true;
		}catch(Exception e) {
			System.out.println(e+"ERROR");
			return false;
		}
		
	}
	public static void DonedActivity(int id) {
		try {
			Connection conn = MyConnection.getConnection();
			/*Statement st =conn.createStatement();
			st.executeUpdate("INSERT INTO to_do_list (list,is_done) values ("+act+","+0+")");*/
			PreparedStatement ps = conn.prepareStatement("UPDATE to_do_list set is_done=1 where id=?");
			ps.setInt(1, id);
            ps.execute();
	
		}catch(Exception e) {
			System.out.println(e+"ERROR");
		}
	}
	public static List<TDListBean> getDonedActivities(){
		List<TDListBean> lista = new ArrayList<TDListBean>();
		try {
			Connection conn = MyConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM to_do_list  where is_done=1");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				lista.add(new TDListBean(rs.getString("list"),rs.getInt("id"),rs.getInt("is_done")));
			}
		}catch(Exception e) {
			System.out.println(e+"ERROR");
			return lista;
		}
	

		
		return lista;
	}
}
