<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.softtek.academy.javaweb.controller.TDListBean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.softtek.academy.javaweb.dao.TDListDAO" %>
<%@ page import = "java.util.*" %>  
<%List<TDListBean> acts = TDListDAO.getDonedActivities();
    session.setAttribute("listD", acts);
    String contextPath = request.getContextPath();

%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>DONE ACTIVITIES</h2>
<table style="text-align:center;">
<tr>
	<th>ID</th>
	<th>Description</th>
</tr>
<c:forEach items = '${listD}' var = "act">
<tr>
    <td>${act.getId()}</td>
    <td>${act.getAct()}</td> 
</tr>
</c:forEach>
</table>
</body>
<footer><a href="<%= contextPath %>/views/ShowActivities.jsp">Pending Activities</a> &nbsp;&nbsp;&nbsp;<a href="<%= contextPath %>/views/NewActivity.jsp">Add Activity</a>
</html>